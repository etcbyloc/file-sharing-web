const routesUser = (app) => {
  app.get('/', (req, res) => {
    delete req.session.loggedAdminID;
    res.render('0_homepage', {
      pagetitle: 'Trang chủ',
      stylink: '/1_style/1_user.css',
      loggedRole: req.session.loggedRole,
      loggedUserID: req.session.loggedUserID,
      loggedAdminID: req.session.loggedAdminID,
    });
  });
  

  app.get('/about-us', (req, res) => {
    delete req.session.loggedAdminID;
    res.render('1_about-us', {
      pagetitle: 'Giới thiệu về DuanDocuments - Xu hướng chia sẻ tài liệu khá nhất hiện nay',
      stylink: '/1_style/1_user.css',
      loggedRole: req.session.loggedRole,
      loggedUserID: req.session.loggedUserID,
      loggedAdminID: req.session.loggedAdminID,
    });
  });
  

  app.get('/question', (req, res) => {
    delete req.session.loggedAdminID;
    res.render('1_question', {
      pagetitle: 'Câu hỏi thường gặp',
      stylink: '/1_style/1_user.css',
      loggedRole: req.session.loggedRole,
      loggedUserID: req.session.loggedUserID,
      loggedAdminID: req.session.loggedAdminID,
    });
  });
  

  app.get('/send-contact', (req, res) => {
    delete req.session.loggedAdminID;
    res.render('1_send-contact', {
      pagetitle: 'Liên hệ với chúng tôi',
      stylink: '/1_style/1_user.css',
      loggedRole: req.session.loggedRole,
      loggedUserID: req.session.loggedUserID,
      loggedAdminID: req.session.loggedAdminID,
    });
  });
  

  app.get('/upload', (req, res) => {
    delete req.session.loggedAdminID;
    res.render('2_file-upload', {
      pagetitle: 'Chia sẻ dữ liệu',
      stylink: '/1_style/1_user.css',
      loggedRole: req.session.loggedRole,
      loggedUserID: req.session.loggedUserID,
      loggedAdminID: req.session.loggedAdminID,
      color: '',
      noti: '',
    });
  });
  

  app.get('/file-list', (req, res) => {
    delete req.session.loggedAdminID;
    const mySQL = `SELECT * FROM chiasetailieu.files WHERE NOT fileStatus = 'DELETE' ORDER BY fileDate DESC`;
    req.db.query(mySQL, (err, results) => {
      if (err) throw err;
      res.render('2_file-list', {
        pagetitle: 'Kho tài liệu',
        stylink: '/1_style/1_user.css',
        loggedRole: req.session.loggedRole,
        loggedUserID: req.session.loggedUserID,
        loggedAdminID: req.session.loggedAdminID,
        files: results,
        keyword: '',
      });
    });
  });
  

  app.get('/file-list/:fileClass', (req, res) => {
    delete req.session.loggedAdminID;
    const fileClass = req.params.fileClass;
    let classFile = "";
    if (fileClass === "giao-trinh") {
      classFile = "Giáo trình";
    }
    else if (fileClass === "bai-tap") {
      classFile = "Bài tập";
    }
    else if (fileClass === "de-thi") {
      classFile = "Đề thi";
    }
    else if (fileClass === "tutorial") {
      classFile = "Tutorial";
    }
    else {};
    const mySQL = `SELECT * FROM chiasetailieu.files WHERE fileClass = ? AND NOT fileStatus = 'DELETE' ORDER BY fileDate DESC`;
    req.db.query(mySQL, [classFile], (err, results) => {
      if (err) throw err;
      res.render('2_file-list', {
        pagetitle: `Kho tài liệu ${'theo ' + classFile}`,
        stylink: '/1_style/1_user.css',
        loggedRole: req.session.loggedRole,
        loggedUserID: req.session.loggedUserID,
        loggedAdminID: req.session.loggedAdminID,
        files: results,
        keyword: `theo thể loại ${classFile}`,
      });
    });
  });


  app.get('/file-detail/:fileID', (req, res) => {
    delete req.session.loggedAdminID;
    console.log(req.params.fileID);
    const mySQL1 = `SELECT * FROM chiasetailieu.files WHERE fileID = ? AND NOT fileStatus = 'DELETE'`;
    const mySQL2 = `SELECT * FROM chiasetailieu.fileComments WHERE fileID = ? AND NOT commentStatus = 'DELETE' ORDER BY commentDate DESC`
    req.db.query(mySQL1, [req.params.fileID], (err, results1) => {
      if (err) throw err;
      if (results1.length !== 0) {
        console.log(results1);
        req.db.query(mySQL2, [req.params.fileID], (err, results2) => {
          if (err) throw err;
          // console.log(results2);
          res.render('2_file-detail', {
            pagetitle: results1[0].fileName,
            stylink: '/1_style/1_user.css',
            fileInfo: results1[0],
            loggedRole: req.session.loggedRole,
            loggedUserID: req.session.loggedUserID,
            loggedAdminID: req.session.loggedAdminID,
            comments: results2,
          });
        });
      }
      else {
        console.log(results1);
        res.render('2_file-detail-not-found', {
          pagetitle: 'Không tìm thấy tài liệu',
          stylink: '/1_style/1_user.css',
          loggedRole: req.session.loggedRole,
          loggedUserID: req.session.loggedUserID,
          loggedAdminID: req.session.loggedAdminID,
        });
      };
    });
  });


  app.get('/login', (req, res) => {
    delete req.session.loggedAdminID;
    res.render('3_login', {
      pagetitle: 'Đăng nhập',
      stylink: '/1_style/1_user.css',
      noti: '',
      loggedRole: req.session.loggedRole,
      loggedUserID: req.session.loggedUserID,
      loggedAdminID: req.session.loggedAdminID,
    });
  });
  

  app.get('/register', (req, res) => {
    delete req.session.loggedAdminID;
    res.render('3_register', {
      pagetitle: 'Đăng ký tài khoản',
      stylink: '/1_style/1_user.css',
      noti: '',
      loggedRole: req.session.loggedRole,
      loggedUserID: req.session.loggedUserID,
      loggedAdminID: req.session.loggedAdminID,
    });
  });
  

  app.get('/user-info', (req, res) => {
    delete req.session.loggedAdminID;
    const mySQL = `SELECT * FROM chiasetailieu.users WHERE userID = ?`;
    req.db.query(mySQL, [req.session.loggedUserID], (err, results) => {
      if (err) throw err;
      console.log(results);
      res.render('4_user-info', {
        pagetitle: 'Thông tin người dùng',
        stylink: '/1_style/1_user.css',
        info: results[0],
        loggedRole: req.session.loggedRole,
        loggedUserID: req.session.loggedUserID,
        loggedAdminID: req.session.loggedAdminID,
      });
    });
  });
  

  app.get('/user-uploaded-file', (req, res) => {
    delete req.session.loggedAdminID;
    const mySQL = `SELECT * FROM chiasetailieu.files WHERE userID = ? AND NOT fileStatus = 'DELETE' ORDER BY fileDate DESC`;
    req.db.query(mySQL, [req.session.loggedUserID], (err, results) => {
      if (err) throw err;
      // console.log(results);
      res.render('4_user-uploaded-file', {
        pagetitle: 'Tài liệu đã tải lên',
        stylink: '/1_style/1_user.css',
        files: results,
        loggedRole: req.session.loggedRole,
        loggedUserID: req.session.loggedUserID,
        loggedAdminID: req.session.loggedAdminID,
      });
    });
  });


  app.get('/user-commented', (req, res) => {
    delete req.session.loggedAdminID;
    const mySQL = `SELECT * FROM chiasetailieu.fileComments WHERE userID = ? AND NOT commentStatus = 'DELETE' ORDER BY commentDate DESC`;
    req.db.query(mySQL, [req.session.loggedUserID], (err, results) => {
      if (err) throw err;
      // console.log(results);
      res.render('4_user-commented', {
        pagetitle: 'Bình luận đã viết',
        stylink: '/1_style/1_user.css',
        comments: results,
        loggedRole: req.session.loggedRole,
        loggedUserID: req.session.loggedUserID,
        loggedAdminID: req.session.loggedAdminID,
      });
    });
  });


  app.get('/logout', (req, res) => {
    req.session.destroy();
    res.redirect('/');
  });


  app.post('/login', (req, res) => {
    const { userName, userPass } = req.body;

    if (!userName || !userPass) {
      res.render('3_login.ejs', {
        pagetitle: 'Đăng nhập',
        stylink: '/1_style/1_user.css',
        noti: 'Bạn chưa điền thông tin đăng nhập!',
        loggedRole: req.session.loggedRole,
        loggedUserID: req.session.loggedUserID,
        loggedAdminID: req.session.loggedAdminID,
      });
    }
    else {
      const mySQL = `SELECT * FROM chiasetailieu.users WHERE userName = ? AND userPassword = ?`;
      req.db.query(mySQL, [userName, userPass], (err, results) => {
        if (err) throw err;
        if (results.length === 1) {
          console.log(results);
          delete req.session.loggedAdminID;
          req.session.loggedRole = "user";
          req.session.loggedUserID = results[0].userID;
          req.session.save(() => {
            console.log(req.session.loggedRole);
            console.log(req.session.loggedUserID);
            res.redirect('/');
          });
        } 
        else {
          console.log(results.length);
          res.render('3_login.ejs', {
            pagetitle: 'Đăng nhập',
            stylink: '/1_style/1_user.css',
            noti: 'Tên đăng nhập hoặc mật khẩu không đúng!',
            loggedRole: req.session.loggedRole,
            loggedUserID: req.session.loggedUserID,
            loggedAdminID: req.session.loggedAdminID, 
          });
        };
      });
    };
  });


  app.post('/register', (req, res) => {
    const { newUser, newName, newEmail, newPhone, newPass, confirmPass } = req.body;

    if (!newUser || !newName || !newEmail || !newEmail.includes('@') || !newPhone || !newPass || !confirmPass) {
      res.render('3_register.ejs', {
        pagetitle: 'Đăng ký tài khoản',
        stylink: '/1_style/1_user.css',
        noti: 'Bạn chưa nhập đầy đủ thông tin!',
        loggedRole: req.session.loggedRole,
        loggedUserID: req.session.loggedUserID,
        loggedAdminID: req.session.loggedAdminID,
      });
    }
    else if (newUser.includes(' ') || newUser.length > 10) {
      res.render('3_register.ejs', {
        pagetitle: 'Đăng ký tài khoản',
        stylink: '/1_style/1_user.css',
        noti: "Tên đăng nhập không được có dấu cách ' ' và có tối đa 16 ký tự !",
        loggedRole: req.session.loggedRole,
        loggedUserID: req.session.loggedUserID,
        loggedAdminID: req.session.loggedAdminID,
      });
    }
    else if (newPass.includes(' ')) {
      res.render('3_register.ejs', {
        pagetitle: 'Đăng ký tài khoản',
        stylink: '/1_style/1_user.css',
        noti: "Mật khẩu không được có dấu cách ' ' !",
        loggedRole: req.session.loggedRole,
        loggedUserID: req.session.loggedUserID,
        loggedAdminID: req.session.loggedAdminID,
      });
    }
    else if (newPass !== confirmPass) {
      res.render('3_register.ejs', {
        pagetitle: 'Đăng ký tài khoản',
        stylink: '/1_style/1_user.css',
        noti: 'Mật khẩu không khớp!',
        loggedRole: req.session.loggedRole,
        loggedUserID: req.session.loggedUserID,
        loggedAdminID: req.session.loggedAdminID,
      });
    }
    else {
      const mySQL1 = `SELECT * FROM chiasetailieu.users WHERE userName = ?`;
      const mySQL2 = `INSERT INTO chiasetailieu.users (userName, userPassword, userFull, userEmail, userPhone) VALUES (?, ?, ?, ?, ?)`;
  
      req.db.query(mySQL1, [newUser], (err, results) => {
        if (err) throw err;
        if (results.length > 0) {
          res.render('3_register.ejs', {
            pagetitle: 'Đăng ký tài khoản',
            stylink: '/1_style/1_user.css',
            noti: 'Tên người dùng đã tồn tại!',
            loggedRole: req.session.loggedRole,
            loggedUserID: req.session.loggedUserID,
            loggedAdminID: req.session.loggedAdminID,
          });
        }
        else {
          req.db.query(mySQL2, [newUser, newPass, newName, newEmail, newPhone], (err, results) => {
            if (err) throw err;
            console.log(results);
            res.redirect('/login');
          });
        };
      });
    };
  });


  app.post('/send-contact', (req, res) => {
    const { yourName, yourEmail, yourNumber, yourNote } = req.body;
    let contactColor = "";
    let contactNoti = "";
    let contactDate = new Date();
    const checkNote = yourNote.split(" ");
    console.log(checkNote);
    const blackList = ["ora", "muda", "moi", "ngu"];
    let warningWord = false;
    if (!yourName || !yourEmail || !yourEmail.includes('@') || !yourNumber || !yourNote) {
      contactColor = "red";
      contactNoti = "Bạn chưa nhập đủ thông tin!";
    }
    else if(yourNote.length < 100) {
      contactColor = "red";
      contactNoti = "Bạn cần nhập ghi chú liên hệ tối thiểu 100 chữ!";
    }
    else {
      checkNote.forEach((word) => {
        if (blackList.includes(word.toLowerCase())) {
          warningWord = true;
        }
      });
      console.log(warningWord);
      if (warningWord) {
        contactColor = "red";
        contactNoti = "Ghi chú liên hệ của bạn có chứa từ nhạy cảm";
      } else {
        const mySQL = `INSERT INTO chiasetailieu.contacts (contactName, contactEmail, contactPhone, contactNote, contactDate)
                      VALUES (?, ?, ?, ?, ?)`;
        req.db.query(mySQL, [yourName, yourEmail, yourNumber, yourNote, contactDate], (err, results) => {
          if (err) throw err;
          console.log(results);
        });
        contactColor = "green";
        contactNoti = "Gửi liên hệ thành công!";
      };
    };
    console.log({ contactColor, contactNoti });
    res.json({ contactColor, contactNoti });
  });


  app.post('/upload', (req, res) => {
    if (req.files) {
      const fileUp = req.files.fileUpload;
      console.log(fileUp);
      const { fileClass, fileDescription } = req.body;
      const userID = req.session.loggedUserID;
      const fileName = fileUp.name;
      const fileDate = new Date();
      let fileSize = fileUp.size;
      if (fileSize >= ( 50 * 1024 * 1024 )) {
        res.render('2_file-upload', {
          pagetitle: 'Chia sẻ dữ liệu',
          stylink: '/1_style/1_user.css',
          loggedRole: req.session.loggedRole,
          loggedUserID: req.session.loggedUserID,
          loggedAdminID: req.session.loggedAdminID,
          color: '1',
          noti: 'Rất tiếc! Kích thước tệp bạn tải lên phải dưới 50MB! :((',
        });
      }
      else {
        fileSize = fileSize / (1024 * 1024);
        const folderSave = `${Date.now()}${Math.floor((Math.random())*9000)+1000}${Math.floor((Math.random())*9000)+1000}`;
        const folderPath = req.fp.join(__dirname, 'file-storage', folderSave);
        req.fs.mkdirSync(folderPath);
        const filePath = req.fp.join(folderPath, fileName);
        fileUp.mv(filePath, (err) => {
          if (err) throw err;
          const mySQL1 = `SELECT userFull FROM chiasetailieu.users WHERE userID = ?`;
          const mySQL2 = `INSERT INTO chiasetailieu.files (userID, fileName, fileClass, fileDate, fileSize, fileUploader, fileDescription, filePath, fileStatus) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)`;
          req.db.query(mySQL1, [userID], (err, results) => {
            if (err) throw err;
            console.log(results);
            const fileUploader = results[0].userFull;
            req.db.query(mySQL2, [userID, fileName, fileClass, fileDate, fileSize, fileUploader, fileDescription, filePath, 'ACTIVE'], (err, results) => {
              if (err) throw err;
              console.log(results);
              res.render('2_file-upload', {
                pagetitle: 'Chia sẻ dữ liệu',
                stylink: '/1_style/1_user.css',
                loggedRole: req.session.loggedRole,
                loggedUserID: req.session.loggedUserID,
                loggedAdminID: req.session.loggedAdminID,
                color: '2',
                noti: 'Đã tải lên thành công!',
              });
            });
          });
        });
      };
    }
    else {
      res.render('2_file-upload', {
        pagetitle: 'Chia sẻ dữ liệu',
        stylink: '/1_style/1_user.css',
        loggedRole: req.session.loggedRole,
        loggedUserID: req.session.loggedUserID,
        loggedAdminID: req.session.loggedAdminID,
        color: '1',
        noti: 'Bạn chưa tải file lên! :((',
      });
    };
  });


  app.post("/user/comment/:commentID/update", (req, res) => {
    res.render('6_user-comment-update', {
      pagetitle: 'Sửa bình luận',
      stylink: '/1_style/1_user.css',
      loggedRole: req.session.loggedRole,
      loggedUserID: req.session.loggedUserID,
      loggedAdminID: req.session.loggedAdminID,
      commentID: req.params.commentID,
    });
  })


  app.post("/user/comment/:commentID/delete", (req, res) => {
    res.render('6_user-comment-delete', {
      pagetitle: 'Xoá bình luận',
      stylink: '/1_style/1_user.css',
      loggedRole: req.session.loggedRole,
      loggedUserID: req.session.loggedUserID,
      loggedAdminID: req.session.loggedAdminID,
      commentID: req.params.commentID,
    });
  })


  app.post("/user/file/:fileID/update", (req, res) => {
    res.render('6_user-file-update', {
      pagetitle: 'Cập nhật tài liệu',
      stylink: '/1_style/1_user.css',
      loggedRole: req.session.loggedRole,
      loggedUserID: req.session.loggedUserID,
      loggedAdminID: req.session.loggedAdminID,
      fileID: req.params.fileID,
    });
  })


  app.post("/user/file/:fileID/delete", (req, res) => {
    res.render('6_user-file-delete', {
      pagetitle: 'Xoá tài liệu',
      stylink: '/1_style/1_user.css',
      loggedRole: req.session.loggedRole,
      loggedUserID: req.session.loggedUserID,
      loggedAdminID: req.session.loggedAdminID,
      fileID: req.params.fileID,
    });
  })
}


module.exports = routesUser;