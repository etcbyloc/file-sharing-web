const routesGeneral = (app) => {
  app.post('/form-comment/:fileID', (req, res) => {
    const { yourComment, yourRate } = req.body;
    const fileID = req.params.fileID;
    let cmtColor = "";
    let cmtNoti = "";
    let cmtDate = new Date();
    const checkComment = yourComment.split(" ");
    console.log(checkComment);
    const blackList = ["ora", "muda", "moi", "ngu"];
    let warningWord = false;
    if (!yourComment || !yourRate) {
      cmtColor = "red";
      cmtNoti = "Bạn chưa nhập bình luận/ chấm điểm đánh giá!";
    }
    else if(yourComment.length < 50) {
      cmtColor = "red";
      cmtNoti = "Bạn cần nhập bình luận tối thiểu 50 chữ!";
    }
    else {
      checkComment.forEach((word) => {
        if (blackList.includes(word.toLowerCase())) {
          warningWord = true;
        }
      });
      console.log(warningWord);
      if (warningWord) {
        cmtColor = "red";
        cmtNoti = "Bình luận của bạn có chứa từ nhạy cảm";
      } else {
        if (req.session.loggedRole === "user" && req.session.loggedUserID) {
          const mySQL1 = `SELECT * FROM chiasetailieu.files WHERE fileID = ?`;
          const mySQL2 = `SELECT * FROM chiasetailieu.users WHERE userID = ?`;
          const mySQL3 = `INSERT INTO chiasetailieu.fileComments (fileID, userID, commentUser, commentName, commentEmail, commentDate, commentCom, commentRate, commentFile, commentStatus)
                          VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`;
          req.db.query(mySQL1, [fileID], (err, results1) => {
            if (err) throw err;
            // console.log(results1);
            let commentFile = results1[0].fileName;
            req.db.query(mySQL2, [req.session.loggedUserID], (err, results2) => {
              if (err) throw err;
              // console.log(results2);
              let userID = req.session.loggedUserID;
              let commentUser = results2[0].userName;
              let commentName = results2[0].userFull;
              let commentEmail = results2[0].userEmail;
              req.db.query(mySQL3, [fileID, userID, commentUser, commentName, commentEmail, cmtDate, yourComment, yourRate, commentFile, "ACTIVE"], (err, results3) => {
                if (err) throw err;
                console.log(results3);
              });
            });
          });
        }
        else if (req.session.loggedRole === "admin" && req.session.loggedAdminID) {
          const mySQL1 = `SELECT * FROM chiasetailieu.files WHERE fileID = ?`;
          const mySQL2 = `SELECT * FROM chiasetailieu.admins WHERE adminID = ?`;
          const mySQL3 = `INSERT INTO chiasetailieu.fileComments (fileID, adminID, commentUser, commentName, commentEmail, commentDate, commentCom, commentRate, commentFile, commentStatus)
                          VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`;
          req.db.query(mySQL1, [fileID], (err, results1) => {
            if (err) throw err;
            // console.log(results1);
            let commentFile = results1[0].fileName;
            req.db.query(mySQL2, [req.session.loggedAdminID], (err, results2) => {
              if (err) throw err;
              // console.log(results2);
              let adminID = req.session.loggedAdminID;
              let commentUser = results2[0].adminName;
              let commentName = results2[0].adminFull;
              let commentEmail = results2[0].adminEmail;
              req.db.query(mySQL3, [fileID, adminID, commentUser, commentName, commentEmail, cmtDate, yourComment, yourRate, commentFile, "ACTIVE"], (err, results3) => {
                if (err) throw err;
                console.log(results3);
              });
            });
          });
        }
        else {};
        cmtColor = "green";
        cmtNoti = "Bình luận thành công!";
      };
    };
    console.log({ cmtColor, cmtNoti });
    res.json({ cmtColor, cmtNoti });
  });


  app.post('/file-download/:fileID', (req, res) => {
    console.log(req.params.fileID);
    const mySQL1 = `SELECT filePath FROM chiasetailieu.files WHERE fileID = ? AND NOT fileStatus = 'DELETE'`;
    req.db.query(mySQL1, [req.params.fileID], (err, results) => {
      if (err) throw err;
      // console.log(results);
      const filePath = results[0].filePath;
      res.download(filePath, (err) => {
        if (err) throw err;
      });
    });
  });


  app.post("/update-comment/:commentID", (req, res) => {
    const commentID = req.params.commentID;
    const { newComment, newRate } = req.body;
    let statusColor = "";
    let statusNoti = "";
    const checkComment = newComment.split(" ");
    console.log(checkComment);
    const blackList = ["ora", "muda", "moi", "ngu"];
    let warningWord = false;
    if (!newComment || !newRate) {
      statusColor = "red";
      statusNoti = "Bạn chưa nhập bình luận mới/ chấm điểm đánh giá mới!";
    }
    else if(newComment.length < 50) {
      statusColor = "red";
      statusNoti = "Bạn cần nhập bình luận mới tối thiểu 50 chữ!";
    }
    else {
      checkComment.forEach((word) => {
        if (blackList.includes(word.toLowerCase())) {
          warningWord = true;
        }
      });
      console.log(warningWord);
      if (warningWord) {
        statusColor = "red";
        statusNoti = "Bình luận của bạn có chứa từ nhạy cảm";
      } else {
        const mySQL = `UPDATE chiasetailieu.fileComments SET commentCom = ?, commentRate = ? WHERE commentID = ?`;
        req.db.query(mySQL, [newComment, newRate, commentID], (err, results) => {
          if (err) throw err;
        });
        statusColor = "green";
        statusNoti = "Cập nhật thành công! Bạn hãy tự quay lại trang trước!";
        console.log({ statusColor, statusNoti });
      };
    };
    console.log({ statusColor, statusNoti });
    res.json({ statusColor, statusNoti });
  })


  app.post("/delete-comment/:commentID", (req, res) => {
    const commentID = req.params.commentID;
    mySQL = `UPDATE chiasetailieu.fileComments SET commentStatus = ? WHERE commentID = ?`;
    req.db.query(mySQL, ['DELETE', commentID], (err, results) => {
      if (err) throw err;
      if (req.session.loggedRole === "admin" && req.session.loggedAdminID) {
        res.redirect("/admin/main");
      } else {
        res.redirect("/user-commented");
      }
    });
  })


  app.post("/update-file/:fileID", (req, res) => {
    const fileID = req.params.fileID;
    const { newName, newClass, newDescription } = req.body;
    let statusColor = "";
    let statusNoti = "";
    const checkName = newName.split(" ");
    const checkDescription = newDescription.split(" ");
    console.log(checkName);
    console.log(checkDescription);
    const blackList = ["ora", "muda", "moi", "ngu"];
    let warningWord = false;
    if (!newName || !newClass || !newDescription) {
      statusColor = "red";
      statusNoti = "Bạn chưa nhập điền hết các nội dung mới!";
    }
    else if(newName.length < 5) {
      statusColor = "red";
      statusNoti = "Bạn cần nhập tên mới tối thiểu 5 chữ!";
    }
    else {
      checkName.forEach((word) => {
        if (blackList.includes(word.toLowerCase())) {
          warningWord = true;
        }
      });
      checkDescription.forEach((word) => {
        if (blackList.includes(word.toLowerCase())) {
          warningWord = true;
        }
      });
      console.log(warningWord);
      if (warningWord) {
        statusColor = "red";
        statusNoti = "Nội dung mới của bạn có chứa từ nhạy cảm";
      } else {
        const mySQL1 = `UPDATE chiasetailieu.files SET fileName = ?, fileClass = ?, fileDescription = ? WHERE fileID = ?`;
        const mySQL2 = `UPDATE chiasetailieu.fileComments SET commentFile = ? WHERE fileID = ?`;
        req.db.query(mySQL1, [newName, newClass, newDescription, fileID], (err, results1) => {
          if (err) throw err;
          req.db.query(mySQL2, [newName, fileID], (err, results2) => {
            if (err) throw err;
          });
        });
        statusColor = "green";
        statusNoti = "Cập nhật thành công! Bạn hãy tự quay lại trang trước!";
        console.log({ statusColor, statusNoti });
      };
    };
    console.log({ statusColor, statusNoti });
    res.json({ statusColor, statusNoti });  
  })


  app.post("/delete-file/:fileID", (req, res) => {
    const fileID = req.params.fileID;
    mySQL1 = `UPDATE chiasetailieu.fileComments SET commentStatus = ? WHERE fileID = ?`;
    mySQL2 = `UPDATE chiasetailieu.files SET fileStatus = ? WHERE fileID = ?`;
    req.db.query(mySQL1, ['DELETE', fileID], (err, results1) => {
      if (err) throw err;
      req.db.query(mySQL2, ['DELETE', fileID], (err, results2) => {
        if (err) throw err;
        if (req.session.loggedRole === "admin" && req.session.loggedAdminID) {
          res.redirect("/admin/file-list");
        } else {
          res.redirect("/file-list");
        }
      });
    });
  })
}


module.exports = routesGeneral;