const scrollToTop = document.getElementById("scroll-to-top");
scrollToTop.addEventListener("click", () => {
  document.documentElement.scrollTop = 0;
})
// documentElement xác định node document element là html
// kết hợp với scrollTop để khi nhấn nút sẽ đưa vị trí cuộn về vị trí theo chiều dọc = 0 (vị trí đầu trang)