document.addEventListener("DOMContentLoaded", () => {
  let advertisement = document.getElementById("advertisement");
  let closeBtn = document.getElementById("closeButton");

  if (document.cookie.indexOf("popupCookie") === -1) {
    setTimeout(() => {
      advertisement.style.display = "block";
    }, 60 * 1000)   // pop-up quảng cáo sẽ đổi style hiển thị từ "none" sang "block" sau 60s kể từ khi mở trang chủ lên
  }                 // điều kiện hiện pop-up là cookie "popupCookie" được đánh chỉ số là -1 (khi cookie đó chưa xuất hiện)

  closeBtn.addEventListener("click", () => {
    document.cookie = "popupCookie=true; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";
                    // đóng quảng cáo làm kích hoạt cookie "popupCookie" cho trang chủ với thời gian hết hạn trong tương lai xa
    advertisement.style.display = "none";
  });
});