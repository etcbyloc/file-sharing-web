const routesAdmin = (app) => {
  app.get('/admin', (req, res) => {
    delete req.session.loggedUserID;
    if (req.session.loggedRole = "admin" && req.session.loggedAdminID) {
      res.redirect('/admin/main');
    }
    else {
      res.redirect('/admin/login');
    }
  });


  app.get('/admin/login', (req, res) => {
    delete req.session.loggedUserID;
    res.render('5_admin-login', {
      pagetitle: 'Đăng nhập tài khoản Admin',
      stylink: '/1_style/0_admin.css',
      loggedRole: req.session.loggedRole,
      loggedUserID: req.session.loggedUserID,
      loggedAdminID: req.session.loggedAdminID,
      noti: '',
    });
  });
  

  app.get('/admin/main', (req, res) => {
    delete req.session.loggedUserID;
    if (!req.session.loggedAdminID) {
      res.redirect('/admin/login');
    }
    else {
      const mySQL1 = `SELECT * FROM chiasetailieu.admins WHERE adminID = ?`;
      const mySQL2 = `SELECT * FROM chiasetailieu.fileComments WHERE NOT commentStatus = 'DELETE' ORDER BY commentDate DESC`;
      req.db.query(mySQL1, [req.session.loggedAdminID], (err, results1) => {
        if (err) throw err;
        console.log(results1);
        req.db.query(mySQL2, (err, results2) => {
          if (err) throw err;
          // console.log(results2);
          res.render('5_admin-main', {
            pagetitle: 'Tổng quan',
            stylink: '/1_style/0_admin.css',
            adminName: results1[0].adminName,
            viewCount: req.vc,
            comments: results2,
            loggedRole: req.session.loggedRole,
            loggedUserID: req.session.loggedUserID,
            loggedAdminID: req.session.loggedAdminID,
          });
        });
      });
    }
  });


  app.get('/admin/file-list', (req, res) => {
    delete req.session.loggedUserID;
    const mySQL = `SELECT * FROM chiasetailieu.files WHERE NOT fileStatus = 'DELETE' ORDER BY fileDate DESC`;
    req.db.query(mySQL, (err, results) => {
      if (err) throw err;
      res.render('5_admin-file-list', {
        pagetitle: 'Kho tài liệu',
        stylink: '/1_style/0_admin.css',
        loggedRole: req.session.loggedRole,
        loggedUserID: req.session.loggedUserID,
        loggedAdminID: req.session.loggedAdminID,
        files: results,
        keyword: '',
      });
    });
  });


  app.get('/admin/file-list/:fileClass', (req, res) => {
    delete req.session.loggedUserID;
    const fileClass = req.params.fileClass;
    let classFile = "";
    if (fileClass === "giao-trinh") {
      classFile = "Giáo trình";
    }
    else if (fileClass === "bai-tap") {
      classFile = "Bài tập";
    }
    else if (fileClass === "de-thi") {
      classFile = "Đề thi";
    }
    else if (fileClass === "tutorial") {
      classFile = "Tutorial";
    }
    else {};
    const mySQL = `SELECT * FROM chiasetailieu.files WHERE fileClass = ? AND NOT fileStatus = 'DELETE' ORDER BY fileDate DESC`;
    req.db.query(mySQL, [classFile], (err, results) => {
      if (err) throw err;
      res.render('5_admin-file-list', {
        pagetitle: `Kho tài liệu ${'theo ' + classFile}`,
        stylink: '/1_style/0_admin.css',
        loggedRole: req.session.loggedRole,
        loggedUserID: req.session.loggedUserID,
        loggedAdminID: req.session.loggedAdminID,
        files: results,
        keyword: `theo thể loại ${classFile}`,
      });
    });
  });


  app.get('/admin/file-detail/:fileID', (req, res) => {
    delete req.session.loggedUserID;
    console.log(req.params.fileID);
    const mySQL1 = `SELECT * FROM chiasetailieu.files WHERE fileID = ? AND NOT fileStatus = 'DELETE'`;
    const mySQL2 = `SELECT * FROM chiasetailieu.fileComments WHERE fileID = ? AND NOT commentStatus = 'DELETE' ORDER BY commentDate DESC`
    req.db.query(mySQL1, [req.params.fileID], (err, results1) => {
      if (err) throw err;
      if (results1.length !== 0) {
        // console.log(results1);
        req.db.query(mySQL2, [req.params.fileID], (err, results2) => {
          if (err) throw err;
          // console.log(results2);
          res.render('5_admin-file-detail', {
            pagetitle: results1[0].fileName,
            stylink: '/1_style/0_admin.css',
            fileInfo: results1[0],
            loggedRole: req.session.loggedRole,
            loggedUserID: req.session.loggedUserID,
            loggedAdminID: req.session.loggedAdminID,
            comments: results2,
          });
        });
      }
      else {
        console.log(results1);
        res.render('5_admin-file-detail-not-found', {
          pagetitle: 'Không tìm thấy tài liệu',
          stylink: '/1_style/0_admin.css',
          loggedRole: req.session.loggedRole,
          loggedUserID: req.session.loggedUserID,
          loggedAdminID: req.session.loggedAdminID,
        });
      };
    });
  });


  app.get('/admin/upload', (req, res) => {
    delete req.session.loggedUserID;
    res.render('5_admin-upload', {
      pagetitle: 'Tải tài liệu',
      stylink: '/1_style/0_admin.css',
      loggedRole: req.session.loggedRole,
      loggedUserID: req.session.loggedUserID,
      loggedAdminID: req.session.loggedAdminID,
      color: '', 
      noti: '',
    });
  });


  app.get('/admin/logout', (req, res) => {
    req.session.destroy();
    res.redirect('/admin/login');
  });


  app.post('/admin/login', (req, res) => {
    const { adminName, adminPass } = req.body;

    if (!adminName || !adminPass) {
      res.render('5_admin-login.ejs', {
        pagetitle: 'Đăng nhập tài khoản Admin',
        stylink: '/1_style/0_admin.css',
        loggedRole: req.session.loggedRole,
        loggedUserID: req.session.loggedUserID,
        loggedAdminID: req.session.loggedAdminID,
        noti: 'Bạn chưa điền thông tin đăng nhập!',
      });
    }
    else {
      const mySQL = `SELECT * FROM chiasetailieu.admins WHERE adminName = ? AND adminPassword = ?`;
      req.db.query (mySQL, [adminName, adminPass], (err, results) => {
        if (err) throw err;
        if (results.length === 1) {
          console.log(results);
          delete req.session.loggedUserID;
          req.session.loggedRole = "admin";
          req.session.loggedAdminID = results[0].adminID;
          req.session.save(() => {
            console.log(req.session.loggedRole);
            console.log(req.session.loggedAdminID);
            res.redirect('/admin/main');
          });
        } 
        else {
          console.log(results.length);
          res.render('5_admin-login.ejs', {
            pagetitle: 'Đăng nhập tài khoản Admin',
            stylink: '/1_style/0_admin.css',
            loggedRole: req.session.loggedRole,
            loggedUserID: req.session.loggedUserID,
            loggedAdminID: req.session.loggedAdminID,
            noti: 'Tên đăng nhập hoặc mật khẩu không đúng!',
          });
        };
      });
    };
  });


  app.post('/admin/upload', (req, res) => {
    if (req.files) {
      const fileUp = req.files.fileUpload;
      console.log(fileUp);
      const { fileClass, fileDescription } = req.body;
      const adminID = req.session.loggedAdminID;
      const fileName = fileUp.name;
      const fileDate = new Date();
      let fileSize = fileUp.size;
      if (fileSize >= ( 50 * 1024 * 1024 )) {
        res.render('5_admin-upload', {
          pagetitle: 'Tải tài liệu',
          stylink: '/1_style/0_admin.css',
          loggedRole: req.session.loggedRole,
          loggedUserID: req.session.loggedUserID,
          loggedAdminID: req.session.loggedAdminID,
          color: '1',
          noti: 'Rất tiếc! Kích thước tệp Admin tải lên phải dưới 50MB!',
        });
      }
      else {
        fileSize = fileSize / (1024 * 1024);
        const folderSave = `${Date.now()}${Math.floor((Math.random())*9000)+1000}${Math.floor((Math.random())*9000)+1000}`;
        const folderPath = req.fp.join(__dirname, 'file-storage', folderSave);
        req.fs.mkdirSync(folderPath);
        const filePath = req.fp.join(folderPath, fileName);
        fileUp.mv(filePath, (err) => {
          if (err) throw err;
          const mySQL1 = `SELECT adminFull FROM chiasetailieu.admins WHERE adminID = ?`;
          const mySQL2 = `INSERT INTO chiasetailieu.files (adminID, fileName, fileClass, fileDate, fileSize, fileUploader, fileDescription, filePath, fileStatus) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)`;
          req.db.query(mySQL1, [adminID], (err, results) => {
            if (err) throw err;
            console.log(results);
            const fileUploader = results[0].adminFull;
            req.db.query(mySQL2, [adminID, fileName, fileClass, fileDate, fileSize, fileUploader, fileDescription, filePath, 'ACTIVE'], (err, results) => {
              if (err) throw err;
              console.log(results);
              res.render('5_admin-upload', {
                pagetitle: 'Tải tài liệu',
                stylink: '/1_style/0_admin.css',
                loggedRole: req.session.loggedRole,
                loggedUserID: req.session.loggedUserID,
                loggedAdminID: req.session.loggedAdminID,
                color: '2',
                noti: 'Đã tải lên thành công!',
              });
            });
          });
        });
      };
    }
    else {
      res.render('5_admin-upload', {
        pagetitle: 'Tải tài liệu',
        stylink: '/1_style/0_admin.css',
        loggedRole: req.session.loggedRole,
        loggedUserID: req.session.loggedUserID,
        loggedAdminID: req.session.loggedAdminID,
        color: '1',
        noti: 'Admin chưa tải file lên!',
      });
    };
  });


  app.post("/admin/comment/:commentID/update", (req, res) => {
    res.render('6_admin-comment-update', {
      pagetitle: 'Sửa bình luận',
      stylink: '/1_style/0_admin.css',
      loggedRole: req.session.loggedRole,
      loggedUserID: req.session.loggedUserID,
      loggedAdminID: req.session.loggedAdminID,
      commentID: req.params.commentID,
    });
  })


  app.post("/admin/comment/:commentID/delete", (req, res) => {
    res.render('6_admin-comment-delete', {
      pagetitle: 'Xoá bình luận',
      stylink: '/1_style/0_admin.css',
      loggedRole: req.session.loggedRole,
      loggedUserID: req.session.loggedUserID,
      loggedAdminID: req.session.loggedAdminID,
      commentID: req.params.commentID,
    });
  })


  app.post("/admin/file/:fileID/update", (req, res) => {
    res.render('6_admin-file-update', {
      pagetitle: 'Cập nhật tài liệu',
      stylink: '/1_style/0_admin.css',
      loggedRole: req.session.loggedRole,
      loggedUserID: req.session.loggedUserID,
      loggedAdminID: req.session.loggedAdminID,
      fileID: req.params.fileID,
    });
  })


  app.post("/admin/file/:fileID/delete", (req, res) => {
    res.render('6_admin-file-delete', {
      pagetitle: 'Xoá tài liệu',
      stylink: '/1_style/0_admin.css',
      loggedRole: req.session.loggedRole,
      loggedUserID: req.session.loggedUserID,
      loggedAdminID: req.session.loggedAdminID,
      fileID: req.params.fileID,
    });
  })
}


module.exports = routesAdmin;