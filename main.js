// thiết lập express và cổng cho localhost
const express = require('express');
const app = express();
const PORT = 8080;

let viewCount = 0;


// thiết lập middleware file uploading
const fileUpload = require('express-fileupload');

app.use(fileUpload({
  limits: { fileSize: 50 * 1024 * 1024 },
  useTempFiles : true,
  tempFileDir : "/tmp/",
}));


// chúng em xin phép dùng thêm middleware 'path' và 'fs' để xử lý đường dẫn tới vị trí lưu file ạ
const path = require('path');
const fs = require('fs');


// thiết lập middleware cookie-parser
const cookieParser = require('cookie-parser');
app.use(cookieParser());


// thiết lập phiên (session)
const session = require('express-session');
const FileStore = require('session-file-store')(session);

app.use(session({
  store: new FileStore({path: './sessions'}),
  secret: 'secret-password',
  cookie: { maxAge: 20 * 60000 },
  saveUninitialized: true,
  resave: false,
}));


// thiết lập CORS
const cors = require('cors');
app.use(cors());


// sử dụng template ejs
app.set('view engine', 'ejs');


// thiết lập tìm các static files
app.use(express.static('client'));


// xử lý dữ liệu URL-encoded khi chúng gửi lên server
app.use(express.urlencoded({extended: true}));


// chạy server

app.listen(PORT, () => {
  console.log(`Working at http://localhost:${PORT}`);
})


// Kết nối CSDL
const mysql = require('mysql2');

const conn = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'mypassword',
  database: 'chiasetailieu'
});

conn.connect((err) => {
  if (err) {
    console.error(`Không thể kết nối tới CSDL: ${err.stack}`);
  } else {
    console.log(`Kết nối thành công!`);
  }
});


// đưa conn, path, fs vào cùng 1 middleware
app.use((req, res, next) => {
  viewCount++;
  req.vc = viewCount;
  req.db = conn;
  req.fp = path;
  req.fs = fs;
  next();
})


// gọi các module cho phía admin và user trong server-side
const routesAdmin = require('./app_admin');

const routesUser = require('./app_user');

const routesGeneral = require('./app_general');

routesAdmin(app);

routesUser(app);

routesGeneral(app);