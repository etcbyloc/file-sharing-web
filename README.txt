Group 8 - Topic 3 - Project
===========================
* Tên đề tài: Xây dựng Website chia sẻ tài liệu
* Các chức năng của chương trình (Chia ra về phía User với phía Admin):
	- Phía User:
		+ Đăng nhập/ Đăng ký tài khoản
		+ Upload tài liệu lên hệ thống
		+ Xem được danh sách tài liệu + danh sách bình luận của riêng User đó đăng tải
		+ Bình luận vào các tài liệu
		+ Cập nhật + Xoá tài liệu và bình luận của chính bản thân User đó đăng tải
		+ Gửi liên hệ/ phản hồi
	- Phía Admin:
		+ Đăng nhập tài khoản (tài khoản được cấp trong CSDL)
		+ Upload tài liệu lên hệ thống
		+ Xem được danh sách tất cả các bình luận đã được đăng tải lên hệ thống ở các trang file (gồm của các Admin và các User)
		+ Bình luận vào các tài liệu
		+ Cập nhật + Xoá tài liệu của chính Admin đó, các Admin khác và của các User
		+ Cập nhật + Xoá bình luận của chính bản thân Admin đó đăng tải
		+ Xoá bình luận của các Admin khác, và của các User

* Hướng dẫn cài đặt:
	- Bước 1: Kiểm tra lại thiết lập MySQL Connection trên máy tính được dùng chạy server
	- Bước 2: Điều chỉnh thông tin kết nối tới MySQL trong file 'main.js' (dòng 69) sao cho phù hợp
	- Bước 3: Mở file 'settingDB.sql' ở ngay trong folder project để cài đặt CSDL 'chiasetailieu'
	cùng các table cần thiết cho hệ thống web (chạy đoạn code từ dòng 4 - hết), ngoài ra còn có các tài khoản được lập sẵn (tên đăng nhập + mật khẩu giống nhau):
		+ User: user1, user2
		+ Admin: admin1, admin2
	- Bước 4: Mở terminal, change directory tới folder dự án ('Group8_Topic3'), rồi gõ node main.js để enjoy ngay dự án này luôn nào!

* Phân công nhiệm vụ: Cả 3 thành viên trong nhóm chúng em (Lộc, Duy và Duẫn) đều cùng nhau đóng góp cho tất cả các phần trong Bài tập lớn này, và mỗi thành viên đều có những nhiệm vụ chính như sau:
	- Hồ Đức Lộc - 20212318:
		+ Xây dựng chính cho phần HTML (về sau là các EJS template).
		+ Xây dựng phần upload tài liệu, lưu trữ và download tài liệu.
		+ Xây dựng file chạy server 'main.js', lồng ghép các phần code của các thành viên khác trong nhóm vào chung 1 thư mục Project của nhóm.
		+ Tham gia đóng góp ý tưởng cho các phần khác cùng các thành viên còn lại trong nhóm.
	- Nguyễn Vũ Anh Duy - 20212293:
		+ Xây dựng chính cho phần CSS của bài tập lớn + Nút cuộn lên đầu trang.
		+ Tham gia đóng góp ý tưởng cho các phần khác cùng các thành viên còn lại trong nhóm.
	- Nguyễn Văn Duẫn - 20212290:
		+ Xây dựng chính cho phần server-side (session, truy vấn CSDL, hiển thị mã nội dung,...) + pop-up và cookie ngăn pop-up quảng cáo.
		+ Tham gia đóng góp ý tưởng cho các phần khác cùng các thành viên còn lại trong nhóm.
	
