DROP DATABASE chiasetailieu;

-- Hãy chạy các dòng code dưới đây trước khi chạy server dự án
CREATE DATABASE chiasetailieu;

CREATE TABLE chiasetailieu.users (
  userID INT NOT NULL AUTO_INCREMENT,
  userName VARCHAR(255) NOT NULL,
  userPassword VARCHAR(255) NOT NULL,
  userFull VARCHAR(255) NOT NULL,
  userEmail VARCHAR(255) NOT NULL,
  userPhone VARCHAR(10) NOT NULL,
  PRIMARY KEY (userID)
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_vietnamese_ci
;

CREATE TABLE chiasetailieu.admins (
  adminID INT NOT NULL AUTO_INCREMENT,
  adminName VARCHAR(255) NOT NULL,
  adminPassword VARCHAR(255) NOT NULL,
  adminFull VARCHAR(255) NOT NULL,
  adminEmail VARCHAR(255) NOT NULL,
  adminPhone VARCHAR(10) NOT NULL,
  PRIMARY KEY (adminID)
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_vietnamese_ci
;

CREATE TABLE chiasetailieu.files (
  fileID INT NOT NULL AUTO_INCREMENT,
  userID INT DEFAULT NULL,
  adminID INT DEFAULT NULL,
  fileName VARCHAR(255) NOT NULL,
  fileClass VARCHAR(255) NOT NULL,
  fileDate DATETIME NOT NULL,
  fileSize DECIMAL(4,2) NOT NULL,
  fileUploader VARCHAR(255) NOT NULL,
  fileDescription VARCHAR(500) NOT NULL,
  filePath VARCHAR(255) NOT NULL,
  fileStatus VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (fileID)
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_vietnamese_ci
;

CREATE TABLE chiasetailieu.contacts (
  contactID INT NOT NULL AUTO_INCREMENT,
  contactName VARCHAR(255) DEFAULT NULL,
  contactEmail VARCHAR(255) DEFAULT NULL,
  contactPhone VARCHAR(255) NOT NULL,
  contactNote VARCHAR(500) NOT NULL,
  contactDate DATETIME NOT NULL,
  PRIMARY KEY (contactID)
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_vietnamese_ci
;

CREATE TABLE chiasetailieu.fileComments (
  commentID INT NOT NULL AUTO_INCREMENT,
  fileID INT DEFAULT NULL,
  userID INT DEFAULT NULL,
  adminID INT DEFAULT NULL,
  commentUser VARCHAR(255) NOT NULL,
  commentName VARCHAR(255) DEFAULT NULL,
  commentEmail VARCHAR(255) DEFAULT NULL,
  commentDate DATETIME NOT NULL,
  commentCom VARCHAR(500) DEFAULT NULL,
  commentRate VARCHAR(255) NOT NULL,
  commentFile VARCHAR(255) NOT NULL,
  commentStatus VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (commentID)
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_vietnamese_ci
;


SET FOREIGN_KEY_CHECKS = 0;

ALTER TABLE chiasetailieu.files
	ADD FOREIGN KEY (userID) REFERENCES users(userID),
    ADD FOREIGN KEY (adminID) REFERENCES admins(adminID);
    
ALTER TABLE chiasetailieu.fileComments
	ADD FOREIGN KEY (fileID) REFERENCES files(fileID),
	ADD FOREIGN KEY (userID) REFERENCES users(userID),
    ADD FOREIGN KEY (adminID) REFERENCES admins(adminID);
    
INSERT INTO chiasetailieu.users (userName, userPassword, userFull, userEmail, userPhone) 
VALUES 
	('user1', 'user1', 'Người dùng 1', 'user1@sis.hust.edu.vn', '0123456789'),
    ('user2', 'user2', 'Người dùng 2', 'user2@sis.hust.edu.vn', '0987654321');
    
INSERT INTO chiasetailieu.admins (adminName, adminPassword, adminFull, adminEmail, adminPhone) 
VALUES 
	('admin1', 'admin1', 'Admin 1', 'admin1@sis.hust.edu.vn', '0246813579'),
    ('admin2', 'admin2', 'Admin 2', 'admin2@sis.hust.edu.vn', '0135792468');